package com.example.springPractice.repository;

import com.example.springPractice.model.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Connects to a Database
 */
@Repository
public interface EmployeeRepository extends MongoRepository<Employee, String> {

}
