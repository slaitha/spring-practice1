package com.example.springPractice.controller;

import com.example.springPractice.model.Employee;
import com.example.springPractice.service.EmployeeService;
import com.example.springPractice.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Handles all the HTTP requests
 */

@RestController
@RequestMapping("/api")
public class EmployeeController {
    @Autowired
    EmployeeService service;

    @GetMapping("/employee/all")
    public ResponseEntity<List<Employee>> getAllEmployees() {
       return ResponseEntity.ok(service.findAll());
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> findById(@PathVariable String id) {
        return ResponseEntity.ok(service.findOneById(id));
    }

    @PostMapping("/employee/one/insert")
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        System.out.println("NEW EMPLOYEE");
        service.insert(employee);
        return new ResponseEntity(employee, HttpStatus.CREATED);
    }

    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Employee> deleteEmployee(@PathVariable String id) {
        service.delete(id);
        return new ResponseEntity("Employee " + id + " Has Been removed", HttpStatus.ACCEPTED);
    }

}
