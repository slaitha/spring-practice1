package com.example.springPractice.service;

import com.example.springPractice.model.Employee;

import java.util.List;

public interface IEmployeeService {
    List<Employee> findAll();
    Employee findOneById(String id);
    void insert(Employee employee);
    void delete(String id);
}
