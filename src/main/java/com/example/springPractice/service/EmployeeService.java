package com.example.springPractice.service;

import com.example.springPractice.model.Employee;
import com.example.springPractice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Handle all Business Logics
 */
@Service
public class EmployeeService implements IEmployeeService {
    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> findAll() {
        return repository.findAll();
    }

    @Override
    public Employee findOneById(String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public void insert(Employee employee) {
        this.repository.insert(employee);
    }

    @Override
    public void delete(String id) {
        this.repository.deleteById(id);
    }
}
